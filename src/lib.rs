use std::{
    borrow::Cow,
    mem::MaybeUninit,
    path::Path,
    ptr::NonNull,
    sync::atomic::{AtomicU64, Ordering},
};

use ahash::AHashMap;
use anyhow::Ok;
use nix::sys::mman::ProtFlags;
use object::{
    Object, ObjectSection, ObjectSymbol, ObjectSymbolTable, RelocationTarget, SectionIndex,
    SectionKind, SymbolIndex, SymbolKind,
};

pub struct Loader<'a> {
    objects: Vec<Cow<'a, [u8]>>,
}

impl<'a> Loader<'a> {
    pub fn new() -> Loader<'a> {
        Loader {
            objects: Default::default(),
        }
    }

    pub fn add_object(&mut self, buf: impl Into<Cow<'a, [u8]>>) {
        self.objects.push(buf.into());
    }

    #[allow(dead_code)]
    pub fn add_object_from_path(&mut self, path: impl AsRef<Path>) -> anyhow::Result<()> {
        self.add_object(Cow::Owned(std::fs::read(path)?));
        Ok(())
    }

    pub fn finish(&self) -> anyhow::Result<LoadedModule> {
        let files = self
            .objects
            .iter()
            .map(|buf| object::File::parse(buf.as_ref()))
            .collect::<object::Result<Vec<_>>>()?
            .into();
        let mut loader = LoadedModule::new();
        loader.load(&files)?;
        Ok(loader)
    }
}

/// Index of the object file, in the "arrival" order.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct ObjectIndex(usize);

impl From<usize> for ObjectIndex {
    fn from(value: usize) -> Self {
        ObjectIndex(value)
    }
}

impl From<ObjectIndex> for usize {
    fn from(value: ObjectIndex) -> Self {
        value.0
    }
}

/// Section key: object index + section index
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct SectionKey(ObjectIndex, SectionIndex);

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct SpanIndex(usize);

impl From<usize> for SpanIndex {
    fn from(value: usize) -> Self {
        SpanIndex(value)
    }
}

impl From<SpanIndex> for usize {
    fn from(value: SpanIndex) -> Self {
        value.0
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct SpanSectionIndex(usize);

impl From<usize> for SpanSectionIndex {
    fn from(value: usize) -> Self {
        SpanSectionIndex(value)
    }
}

impl From<SpanSectionIndex> for usize {
    fn from(value: SpanSectionIndex) -> Self {
        value.0
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
struct SymbolKey(ObjectIndex, SymbolIndex);

/// Contigious range of mmap'ed pages containing sections of the same kind,
/// sharing the same access mode (read, read-write, read-exec).
#[derive(Debug)]
struct PageSpan {
    kind: SectionKind,
    address: u64,
    module_offset: u64,
    size: u64,
    sections: IndexedVec<SpanSectionIndex, SectionEntry>,
}

impl PageSpan {
    pub fn new(kind: SectionKind) -> PageSpan {
        PageSpan {
            kind,
            address: 0,
            module_offset: 0,
            size: 0,
            sections: IndexedVec::new(),
        }
    }

    pub fn add_section(
        &mut self,
        key: SectionKey,
        section: &object::read::Section<'_, '_>,
    ) -> SpanSectionIndex {
        self.sections.push(SectionEntry {
            key,
            kind: section.kind(),
            alignment: section.align(),
            address: 0,
            module_offset: 0,
            span_offset: 0,
            size: section.size(),
        })
    }
}

#[derive(Debug)]
struct SectionEntry {
    key: SectionKey,
    kind: SectionKind,
    alignment: u64,
    address: u64,
    module_offset: u64,
    span_offset: u64,
    size: u64,
}

#[derive(Debug, Clone)]
struct SymbolEntry {
    name: String,
    _key: SymbolKey,
    _section_offset: u64,
    _size: u64,
    _kind: SymbolKind,
    section_key: Option<SectionKey>,
    _module_offset: Option<u64>,
    address: Option<u64>,
}

#[derive(Debug)]
pub struct LoadedModule {
    spans: IndexedVec<SpanIndex, PageSpan>,
    span_map: AHashMap<SectionKind, SpanIndex>,
    sections: AHashMap<SectionKey, (SpanIndex, SpanSectionIndex)>,
    symbols: AHashMap<SymbolKey, SymbolEntry>,
    symbols_by_name: AHashMap<String, Vec<SymbolKey>>,
    module_size: usize,
    buffer: Option<memmap2::MmapMut>,
}

impl LoadedModule {
    pub fn new() -> LoadedModule {
        LoadedModule {
            spans: IndexedVec::new(),
            span_map: Default::default(),
            sections: Default::default(),
            symbols: Default::default(),
            symbols_by_name: Default::default(),
            module_size: 0,
            buffer: None,
        }
    }

    pub fn find_symbol(&self, name: &str, section_kind: Option<SectionKind>) -> Option<u64> {
        let candidates = self.symbols_by_name.get(name)?;
        for symbol_key in candidates {
            let entry = match self.symbols.get(symbol_key) {
                Some(e) => e,
                None => continue,
            };
            let address = match entry.address {
                Some(a) => a,
                None => continue,
            };
            if let Some(section_kind) = section_kind {
                let section = match entry
                    .section_key
                    .and_then(|section_key| self.get_section(section_key))
                {
                    Some(section) => section,
                    None => continue,
                };
                if section.kind == section_kind {
                    return Some(address);
                }
            } else {
                return Some(address);
            }
        }
        None
    }

    pub unsafe fn find_function<F>(&self, name: &str) -> Option<F> {
        let address = self.find_symbol(name, Some(SectionKind::Text))?;
        if address == 0 {
            return None;
        }
        Some(address_as_function(address))
    }

    pub fn list_code_symbols(&self) -> AHashMap<String, u64> {
        self.symbols_by_name
            .keys()
            .filter_map(|name| {
                self.find_symbol(name, Some(SectionKind::Text))
                    .map(|address| (name.clone(), address))
            })
            .collect()
    }

    pub fn module_size(&self) -> usize {
        self.module_size
    }

    pub fn print_spans(&self) {
        println!("{:#?}", self.spans);
    }

    fn get_section(&self, key: SectionKey) -> Option<&SectionEntry> {
        self.sections
            .get(&key)
            .map(|&(span_idx, span_section_idx)| &self.spans[span_idx].sections[span_section_idx])
    }

    fn load(
        &mut self,
        objects: &IndexedVec<ObjectIndex, object::read::File<'_>>,
    ) -> anyhow::Result<()> {
        self.prepare_layout(objects)?;
        self.populate_sections(objects)?;
        self.collect_symbols(objects);
        self.patch_relocations(objects)?;
        self.protect()?;
        Ok(())
    }

    fn prepare_layout(
        &mut self,
        objects: &IndexedVec<ObjectIndex, object::read::File<'_>>,
    ) -> anyhow::Result<()> {
        const SECTION_LIST: &[SectionKind] = &[
            SectionKind::Text,
            SectionKind::ReadOnlyData,
            SectionKind::ReadOnlyString,
            SectionKind::Data,
            SectionKind::UninitializedData,
            SectionKind::Common,
        ];

        for &section_kind in SECTION_LIST {
            for (obj_idx, object) in objects.iter() {
                for section in object.sections().filter(|s| s.kind() == section_kind) {
                    let span_index = *self
                        .span_map
                        .entry(section_kind)
                        .or_insert_with(|| self.spans.push(PageSpan::new(section_kind)));

                    let section_key = SectionKey(obj_idx, section.index());
                    let span_section_index =
                        self.spans[span_index].add_section(section_key, &section);
                    self.sections
                        .insert(section_key, (span_index, span_section_index));
                }
            }
        }

        self.module_size = self.compute_layout() as usize;
        if self.module_size == 0 {
            anyhow::bail!("Nothing to load");
        }
        self.buffer = Some(memmap2::MmapMut::map_anon(self.module_size)?);
        assert_eq!(
            self.module_size,
            self.buffer.as_mut().expect("buffer").len()
        );
        Ok(())
    }

    fn compute_layout(&mut self) -> u64 {
        let mut module_offset = 0u64;
        for (_, span) in self.spans.iter_mut() {
            span.module_offset = module_offset;
            let mut span_offset = 0;
            for (_, section) in span.sections.iter_mut() {
                span_offset = round_up(span_offset, section.alignment);
                section.span_offset = span_offset;
                section.module_offset = module_offset + span_offset;
                span_offset += section.size;
            }
            let module_start = module_offset;
            module_offset = round_up(module_offset + span_offset, page_size());
            span.size = module_offset - module_start;
        }
        module_offset
    }

    fn collect_symbols(&mut self, objects: &IndexedVec<ObjectIndex, object::read::File<'_>>) {
        for (obj_idx, object) in objects.iter() {
            let sym_tab = match object.symbol_table() {
                Some(t) => t,
                None => continue,
            };
            for sym in sym_tab.symbols() {
                let section_key = match sym.section() {
                    object::SymbolSection::Section(section_index) => {
                        Some(SectionKey(obj_idx, section_index))
                    }
                    _ => None,
                };
                let (module_offset, address) = section_key
                    .and_then(|section_key| self.get_section(section_key))
                    .map(|mapped_section| {
                        (
                            Some(sym.address() + mapped_section.module_offset),
                            Some(sym.address() + mapped_section.address),
                        )
                    })
                    .unwrap_or((None, None));

                let key = SymbolKey(obj_idx, sym.index());
                let name = sym.name().unwrap_or_default().to_string();
                let entry = SymbolEntry {
                    name: name.clone(),
                    _key: key,
                    _section_offset: sym.address(),
                    _size: sym.size(),
                    _kind: sym.kind(),
                    section_key,
                    _module_offset: module_offset,
                    address,
                };
                self.symbols.insert(key, entry);
                if !name.is_empty() {
                    self.symbols_by_name
                        .entry(name)
                        .or_insert_with(|| Vec::new())
                        .push(key);
                }
            }
        }
    }

    fn populate_sections(
        &mut self,
        objects: &IndexedVec<ObjectIndex, object::read::File<'_>>,
    ) -> anyhow::Result<()> {
        let buffer = self.buffer.as_mut().expect("buffer");
        let module_address = buffer.as_ptr() as u64;
        for (_, span) in self.spans.iter_mut() {
            span.address = span.module_offset + module_address;

            for (_, section) in span.sections.iter_mut() {
                section.address = section.module_offset + module_address;

                let SectionKey(obj_index, section_index) = section.key;
                let obj_section = objects[obj_index]
                    .section_by_index(section_index)
                    .expect("obj section");
                assert_eq!(obj_section.size(), section.size);

                if section.size != 0 {
                    let data = obj_section.data()?;
                    if data.is_empty() {
                        assert!(
                            section.kind == SectionKind::UninitializedData
                                || section.kind == SectionKind::Common
                        );
                        buffer[section.module_offset as usize
                            ..(section.module_offset + section.size) as usize]
                            .fill(0);
                    } else {
                        buffer[section.module_offset as usize
                            ..(section.module_offset + section.size) as usize]
                            .copy_from_slice(obj_section.data()?);
                    }
                }
            }
        }
        Ok(())
    }

    fn patch_relocations(
        &mut self,
        objects: &IndexedVec<ObjectIndex, object::read::File<'_>>,
    ) -> anyhow::Result<()> {
        let mut buffer = self.buffer.take().expect("buffer");
        for (_, span) in self.spans.iter() {
            if span.kind != SectionKind::Text
                && span.kind != SectionKind::ReadOnlyData
                && span.kind != SectionKind::ReadOnlyDataWithRel
            {
                continue;
            }
            self.patch_relocations_in_span(objects, span, &mut *buffer)?;
        }
        self.buffer = Some(buffer);
        Ok(())
    }

    fn patch_relocations_in_span(
        &self,
        objects: &IndexedVec<ObjectIndex, object::read::File<'_>>,
        span: &PageSpan,
        buffer: &mut [u8],
    ) -> anyhow::Result<()> {
        for (_, section) in span.sections.iter() {
            self.patch_relocations_in_section(objects, section, buffer)?;
        }
        Ok(())
    }

    fn patch_relocations_in_section(
        &self,
        objects: &IndexedVec<ObjectIndex, object::read::File<'_>>,
        section: &SectionEntry,
        buffer: &mut [u8],
    ) -> anyhow::Result<()> {
        let SectionKey(obj_index, section_index) = section.key;
        let obj_section = objects[obj_index].section_by_index(section_index)?;
        for (section_offset, relocation) in obj_section.relocations() {
            match relocation.kind() {
                object::RelocationKind::Relative | object::RelocationKind::PltRelative => (),
                _ => anyhow::bail!(
                    "Relocation kind {:?} is not yet implemented",
                    relocation.kind()
                ),
            }
            if relocation.size() != 32 {
                anyhow::bail!(
                    "Relocation size {} is not yet implemented",
                    relocation.size()
                );
            }
            let target_address = self.resolve_relocation_target(obj_index, relocation.target())?;
            let source_address = section.address + section_offset;
            let buffer_pos = (section.module_offset + section_offset) as usize;
            let value = get_relative_jump(source_address, target_address)?
                .checked_add(relocation.addend() as i32)
                .ok_or_else(|| anyhow::anyhow!("Invalid addend in {relocation:?}"))?;
            buffer[buffer_pos..buffer_pos + 4].copy_from_slice(value.to_le_bytes().as_slice());
        }
        Ok(())
    }

    fn resolve_relocation_target(
        &self,
        obj_index: ObjectIndex,
        target: RelocationTarget,
    ) -> anyhow::Result<u64> {
        let sym_idx = match target {
            object::RelocationTarget::Symbol(sym_idx) => sym_idx,
            _ => anyhow::bail!("Relocation target {:?} is not yet implemented", target),
        };
        let symbol_key = SymbolKey(obj_index, sym_idx);
        let symbol = self
            .symbols
            .get(&symbol_key)
            .ok_or_else(|| anyhow::anyhow!("Unknown symbol key {symbol_key:?}"))?;
        if let Some(address) = symbol.address {
            return Ok(address);
        }
        if !symbol.name.is_empty() {
            if let Some(address) = self.find_symbol(&symbol.name, None) {
                return Ok(address);
            }
        }
        Err(anyhow::anyhow!("Failed to resolve {symbol:?}"))
    }

    fn protect(&mut self) -> anyhow::Result<()> {
        assert!(self.buffer.is_some());
        for (_, span) in self.spans.iter() {
            if span.size == 0 {
                continue;
            }
            let flags = match span.kind {
                SectionKind::Text => ProtFlags::PROT_READ | ProtFlags::PROT_EXEC,
                SectionKind::Data | SectionKind::UninitializedData | SectionKind::Common => {
                    ProtFlags::PROT_READ | ProtFlags::PROT_WRITE
                }
                SectionKind::ReadOnlyData | SectionKind::ReadOnlyString => ProtFlags::PROT_READ,
                _ => panic!("Unexpected span kind {:?}", span.kind),
            };
            unsafe {
                nix::sys::mman::mprotect(
                    NonNull::new(span.address as _).unwrap(),
                    span.size as usize,
                    flags,
                )?;
            }
        }
        Ok(())
    }
}

fn round_up(value: u64, block_size: u64) -> u64 {
    assert_ne!(block_size, 0);
    assert!(block_size.is_power_of_two());
    value
        .checked_add(block_size - 1)
        .expect("add (block size - 1)")
        & (!(block_size - 1))
}

fn get_relative_jump(source_address: u64, target_address: u64) -> anyhow::Result<i32> {
    let (diff, sign) = if source_address <= target_address {
        (target_address - source_address, 1i32)
    } else {
        (source_address - target_address, -1i32)
    };
    if diff >= i32::MAX as u64 {
        anyhow::bail!(
            "get_relative_jump: too far away, source={}, target={}, diff={}, sign={}",
            source_address,
            target_address,
            diff,
            sign
        );
    }
    Ok((diff as i32) * sign)
}

#[derive(Default, Debug, Clone)]
pub struct IndexedVec<Idx, T>(Vec<T>, std::marker::PhantomData<Idx>);

impl<Idx, T> IndexedVec<Idx, T> {
    pub fn new() -> Self {
        IndexedVec(Default::default(), Default::default())
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}

impl<Idx, T> From<Vec<T>> for IndexedVec<Idx, T> {
    fn from(value: Vec<T>) -> Self {
        IndexedVec(value, Default::default())
    }
}

impl<Idx, T> IndexedVec<Idx, T>
where
    Idx: Copy + From<usize>,
{
    pub fn push(&mut self, value: T) -> Idx {
        let idx = self.0.len().into();
        self.0.push(value);
        idx
    }

    pub fn iter(&self) -> impl Iterator<Item = (Idx, &T)> {
        self.0.iter().enumerate().map(|(i, v)| (i.into(), v))
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = (Idx, &mut T)> {
        self.0.iter_mut().enumerate().map(|(i, v)| (i.into(), v))
    }
}

impl<Idx, T> IndexedVec<Idx, T>
where
    Idx: Copy,
    usize: From<Idx>,
{
    pub fn replace(&mut self, index: Idx, value: T) -> T {
        std::mem::replace(&mut self.0[usize::from(index)], value)
    }
}

impl<Idx, T> std::ops::Index<Idx> for IndexedVec<Idx, T>
where
    usize: From<Idx>,
{
    type Output = T;

    fn index(&self, index: Idx) -> &Self::Output {
        &self.0[usize::from(index)]
    }
}

impl<Idx, T> std::ops::IndexMut<Idx> for IndexedVec<Idx, T>
where
    usize: From<Idx>,
{
    fn index_mut(&mut self, index: Idx) -> &mut Self::Output {
        &mut self.0[usize::from(index)]
    }
}

fn page_size() -> u64 {
    static PAGE_SIZE: AtomicU64 = AtomicU64::new(0);

    match PAGE_SIZE.load(Ordering::Relaxed) {
        0 => {
            let page_size = unsafe { libc::sysconf(libc::_SC_PAGESIZE) as u64 };
            PAGE_SIZE.store(page_size, Ordering::Relaxed);
            page_size
        }
        page_size => page_size,
    }
}

unsafe fn address_as_function<F>(addr: u64) -> F {
    assert_eq!(std::mem::size_of::<F>(), std::mem::size_of::<u64>());
    let mut f = MaybeUninit::<F>::uninit();
    std::ptr::write(f.as_mut_ptr() as *mut u64, addr);
    f.assume_init()
}
